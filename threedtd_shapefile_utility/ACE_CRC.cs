﻿using System;
using System.Windows;

namespace ThreeDTD_ShapeFile_Utility
{
    public class ACE_CRC
    {
        private const UInt64 WIDTH_MASK = 0xFFFFFFFFFFFFFFFFU;

        /// Polynomial width for a given polynomial.
        private const UInt64 CRC64 = 64U;

        private UInt64[] CRC64Table;

        /// Starting remainder value to begin XOR calculations
        private UInt64 mInitialRemainder = 0xFFFFFFFFFFFFFFFFU;

        /// Final value to XOR the CRC with at the end of the calculation.
        private UInt64 mFinalXOR = 0;

        public ACE_CRC()
        {
            CRC64Table = new UInt64[]{
            0x0000000000000000U, 0x7ad870c830358979U, 0xf5b0e190606b12f2U, 0x8f689158505e9b8bU,
            0xc038e5739841b68fU, 0xbae095bba8743ff6U, 0x358804e3f82aa47dU, 0x4f50742bc81f2d04U,
            0xab28ecb46814fe75U, 0xd1f09c7c5821770cU, 0x5e980d24087fec87U, 0x24407dec384a65feU,
            0x6b1009c7f05548faU, 0x11c8790fc060c183U, 0x9ea0e857903e5a08U, 0xe478989fa00bd371U,
            0x7d08ff3b88be6f81U, 0x07d08ff3b88be6f8U, 0x88b81eabe8d57d73U, 0xf2606e63d8e0f40aU,
            0xbd301a4810ffd90eU, 0xc7e86a8020ca5077U, 0x4880fbd87094cbfcU, 0x32588b1040a14285U,
            0xd620138fe0aa91f4U, 0xacf86347d09f188dU, 0x2390f21f80c18306U, 0x594882d7b0f40a7fU,
            0x1618f6fc78eb277bU, 0x6cc0863448deae02U, 0xe3a8176c18803589U, 0x997067a428b5bcf0U,
            0xfa11fe77117cdf02U, 0x80c98ebf2149567bU, 0x0fa11fe77117cdf0U, 0x75796f2f41224489U,
            0x3a291b04893d698dU, 0x40f16bccb908e0f4U, 0xcf99fa94e9567b7fU, 0xb5418a5cd963f206U,
            0x513912c379682177U, 0x2be1620b495da80eU, 0xa489f35319033385U, 0xde51839b2936bafcU,
            0x9101f7b0e12997f8U, 0xebd98778d11c1e81U, 0x64b116208142850aU, 0x1e6966e8b1770c73U,
            0x8719014c99c2b083U, 0xfdc17184a9f739faU, 0x72a9e0dcf9a9a271U, 0x08719014c99c2b08U,
            0x4721e43f0183060cU, 0x3df994f731b68f75U, 0xb29105af61e814feU, 0xc849756751dd9d87U,
            0x2c31edf8f1d64ef6U, 0x56e99d30c1e3c78fU, 0xd9810c6891bd5c04U, 0xa3597ca0a188d57dU,
            0xec09088b6997f879U, 0x96d1784359a27100U, 0x19b9e91b09fcea8bU, 0x636199d339c963f2U,
            0xdf7adabd7a6e2d6fU, 0xa5a2aa754a5ba416U, 0x2aca3b2d1a053f9dU, 0x50124be52a30b6e4U,
            0x1f423fcee22f9be0U, 0x659a4f06d21a1299U, 0xeaf2de5e82448912U, 0x902aae96b271006bU,
            0x74523609127ad31aU, 0x0e8a46c1224f5a63U, 0x81e2d7997211c1e8U, 0xfb3aa75142244891U,
            0xb46ad37a8a3b6595U, 0xceb2a3b2ba0eececU, 0x41da32eaea507767U, 0x3b024222da65fe1eU,
            0xa2722586f2d042eeU, 0xd8aa554ec2e5cb97U, 0x57c2c41692bb501cU, 0x2d1ab4dea28ed965U,
            0x624ac0f56a91f461U, 0x1892b03d5aa47d18U, 0x97fa21650afae693U, 0xed2251ad3acf6feaU,
            0x095ac9329ac4bc9bU, 0x7382b9faaaf135e2U, 0xfcea28a2faafae69U, 0x8632586aca9a2710U,
            0xc9622c4102850a14U, 0xb3ba5c8932b0836dU, 0x3cd2cdd162ee18e6U, 0x460abd1952db919fU,
            0x256b24ca6b12f26dU, 0x5fb354025b277b14U, 0xd0dbc55a0b79e09fU, 0xaa03b5923b4c69e6U,
            0xe553c1b9f35344e2U, 0x9f8bb171c366cd9bU, 0x10e3202993385610U, 0x6a3b50e1a30ddf69U,
            0x8e43c87e03060c18U, 0xf49bb8b633338561U, 0x7bf329ee636d1eeaU, 0x012b592653589793U,
            0x4e7b2d0d9b47ba97U, 0x34a35dc5ab7233eeU, 0xbbcbcc9dfb2ca865U, 0xc113bc55cb19211cU,
            0x5863dbf1e3ac9decU, 0x22bbab39d3991495U, 0xadd33a6183c78f1eU, 0xd70b4aa9b3f20667U,
            0x985b3e827bed2b63U, 0xe2834e4a4bd8a21aU, 0x6debdf121b863991U, 0x1733afda2bb3b0e8U,
            0xf34b37458bb86399U, 0x8993478dbb8deae0U, 0x06fbd6d5ebd3716bU, 0x7c23a61ddbe6f812U,
            0x3373d23613f9d516U, 0x49aba2fe23cc5c6fU, 0xc6c333a67392c7e4U, 0xbc1b436e43a74e9dU,
            0x95ac9329ac4bc9b5U, 0xef74e3e19c7e40ccU, 0x601c72b9cc20db47U, 0x1ac40271fc15523eU,
            0x5594765a340a7f3aU, 0x2f4c0692043ff643U, 0xa02497ca54616dc8U, 0xdafce7026454e4b1U,
            0x3e847f9dc45f37c0U, 0x445c0f55f46abeb9U, 0xcb349e0da4342532U, 0xb1eceec59401ac4bU,
            0xfebc9aee5c1e814fU, 0x8464ea266c2b0836U, 0x0b0c7b7e3c7593bdU, 0x71d40bb60c401ac4U,
            0xe8a46c1224f5a634U, 0x927c1cda14c02f4dU, 0x1d148d82449eb4c6U, 0x67ccfd4a74ab3dbfU,
            0x289c8961bcb410bbU, 0x5244f9a98c8199c2U, 0xdd2c68f1dcdf0249U, 0xa7f41839ecea8b30U,
            0x438c80a64ce15841U, 0x3954f06e7cd4d138U, 0xb63c61362c8a4ab3U, 0xcce411fe1cbfc3caU,
            0x83b465d5d4a0eeceU, 0xf96c151de49567b7U, 0x76048445b4cbfc3cU, 0x0cdcf48d84fe7545U,
            0x6fbd6d5ebd3716b7U, 0x15651d968d029fceU, 0x9a0d8ccedd5c0445U, 0xe0d5fc06ed698d3cU,
            0xaf85882d2576a038U, 0xd55df8e515432941U, 0x5a3569bd451db2caU, 0x20ed197575283bb3U,
            0xc49581ead523e8c2U, 0xbe4df122e51661bbU, 0x3125607ab548fa30U, 0x4bfd10b2857d7349U,
            0x04ad64994d625e4dU, 0x7e7514517d57d734U, 0xf11d85092d094cbfU, 0x8bc5f5c11d3cc5c6U,
            0x12b5926535897936U, 0x686de2ad05bcf04fU, 0xe70573f555e26bc4U, 0x9ddd033d65d7e2bdU,
            0xd28d7716adc8cfb9U, 0xa85507de9dfd46c0U, 0x273d9686cda3dd4bU, 0x5de5e64efd965432U,
            0xb99d7ed15d9d8743U, 0xc3450e196da80e3aU, 0x4c2d9f413df695b1U, 0x36f5ef890dc31cc8U,
            0x79a59ba2c5dc31ccU, 0x037deb6af5e9b8b5U, 0x8c157a32a5b7233eU, 0xf6cd0afa9582aa47U,
            0x4ad64994d625e4daU, 0x300e395ce6106da3U, 0xbf66a804b64ef628U, 0xc5bed8cc867b7f51U,
            0x8aeeace74e645255U, 0xf036dc2f7e51db2cU, 0x7f5e4d772e0f40a7U, 0x05863dbf1e3ac9deU,
            0xe1fea520be311aafU, 0x9b26d5e88e0493d6U, 0x144e44b0de5a085dU, 0x6e963478ee6f8124U,
            0x21c640532670ac20U, 0x5b1e309b16452559U, 0xd476a1c3461bbed2U, 0xaeaed10b762e37abU,
            0x37deb6af5e9b8b5bU, 0x4d06c6676eae0222U, 0xc26e573f3ef099a9U, 0xb8b627f70ec510d0U,
            0xf7e653dcc6da3dd4U, 0x8d3e2314f6efb4adU, 0x0256b24ca6b12f26U, 0x788ec2849684a65fU,
            0x9cf65a1b368f752eU, 0xe62e2ad306bafc57U, 0x6946bb8b56e467dcU, 0x139ecb4366d1eea5U,
            0x5ccebf68aecec3a1U, 0x2616cfa09efb4ad8U, 0xa97e5ef8cea5d153U, 0xd3a62e30fe90582aU,
            0xb0c7b7e3c7593bd8U, 0xca1fc72bf76cb2a1U, 0x45775673a732292aU, 0x3faf26bb9707a053U,
            0x70ff52905f188d57U, 0x0a2722586f2d042eU, 0x854fb3003f739fa5U, 0xff97c3c80f4616dcU,
            0x1bef5b57af4dc5adU, 0x61372b9f9f784cd4U, 0xee5fbac7cf26d75fU, 0x9487ca0fff135e26U,
            0xdbd7be24370c7322U, 0xa10fceec0739fa5bU, 0x2e675fb4576761d0U, 0x54bf2f7c6752e8a9U,
            0xcdcf48d84fe75459U, 0xb71738107fd2dd20U, 0x387fa9482f8c46abU, 0x42a7d9801fb9cfd2U,
            0x0df7adabd7a6e2d6U, 0x772fdd63e7936bafU, 0xf8474c3bb7cdf024U, 0x829f3cf387f8795dU,
            0x66e7a46c27f3aa2cU, 0x1c3fd4a417c62355U, 0x935745fc4798b8deU, 0xe98f353477ad31a7U,
            0xa6df411fbfb21ca3U, 0xdc0731d78f8795daU, 0x536fa08fdfd90e51U, 0x29b7d047efec8728U
        };
        }

        /// <summary>
        /// Returns the Initial Remainder (Seed Value) of the CRC object.
        /// </summary>
        ///
        /// <param name="void"></param>
        ///
        /// <returns>Initial remainder (seed value) for the CRC object</returns>
        public UInt64 getSeed()
        {
            return mInitialRemainder;
        }

        /// <summary>Calculate the CRC of the specified message.</summary>
        ///
        /// <param name="pMessage">Pointer to the message data over
        /// which to calculate the CRC.</param>
        ///
        /// <param name="pNBytes">The number of bytes of data in the
        /// message.</param>
        ///
        /// <param name="pCRC"> Reference for CRC checksum initial remainder and result. </param>
        ///
        /// <exception cref="FRAMEWORK"> Thrown when polynomial is an invalid value of 0.</exception>
        ///
        /// <exception cref="FRAMEWORK"> Thrown when pMessage is NULL.</exception>
        ///
        /// <exception cref="FRAMEWORK"> Thrown when NBytes is greater than MAX_MESSAGE_SIZE</exception>
        ///
        /// <exception cref="FRAMEWORK"> Thrown when NBytes is 0.</exception>
        ///
        /// <returns> Returns true if calculation completed, false if any exceptions were thrown.</returns>
        public Boolean CalculateCRC(Object pMessage, UInt64 NBytes, ref UInt64 CRC)
        {
            Boolean RetVal = false;
            UInt64 lCRC = CRC;

            if(NBytes == 0)
            {
                MessageBox.Show("NBytes is 0.");
            }
            else
            {
                //cast data type
                byte[] Message = (byte[])pMessage;
                int cnt = 0;

                while(NBytes > 0)
                {
                    NBytes--;
                    lCRC = CRC64Table[(lCRC ^ Message[cnt++]) & 0xFFU] ^ (lCRC >> 8);
                }
                
                //Final XOR operation
                lCRC = (lCRC ^ mFinalXOR) & WIDTH_MASK;
                CRC = lCRC;
                RetVal = true;
            }

            return RetVal;
        }
    }
}
