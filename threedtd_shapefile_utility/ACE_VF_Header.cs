﻿using System;
using System.Runtime.InteropServices;

namespace ThreeDTD_ShapeFile_Utility
{
    class ACE_VF_Header
    {
      //  [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Header
        {
            public UInt64 Crc;          ///< CRC64 to verify the file & header content
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
            public byte[] Filename;     ///< Name of the file
            public UInt32 UserData1;    ///< User data is set to 0 and overwritten by a read.
            public UInt32 UserData2;    ///< Second part of user data
            public UInt32 Length;       ///< Length of the file content not counting the header.
        }
    }
}
