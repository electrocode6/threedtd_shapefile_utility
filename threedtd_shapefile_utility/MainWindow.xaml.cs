﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Globalization;

namespace ThreeDTD_ShapeFile_Utility
{
    public struct ObsHeader
    {
        public ObsHeader(UInt64 crc, String fileName, UInt32 userData1, UInt32 userData2, UInt32 length, String validated = "----")
        {
            Crc = crc;
            Filename = fileName;
            UserData1 = userData1;
            UserData2 = userData2;
            Length = length;
            Validated = validated;
        }
        public UInt64 Crc { get; set; }               ///< CRC64 to verify the file & header content
        public String Filename { get; set; }          ///< Name of the file
        public UInt32 UserData1 { get; set; }         ///< User data is set to 0 and overwritten by a read.
        public UInt32 UserData2 { get; set; }         ///< Second part of user data
        public UInt32 Length { get; set; }            ///< Length of the file content not counting the header.
        public String Validated { get; set; }         ///< Indication if validation of source file CRC matched
                                                      ///< saved file CRC, and saved file header CRC matches saved file calculated CRC.
    }
    public struct ObsVersion
    {
        public ObsVersion(UInt16 verMajor = 1, UInt16 verMinor = 0 )
        {
            VerMajor = verMajor;
            VerMinor = verMinor;
        }
        public UInt16 VerMajor { get; set; }
        public UInt16 VerMinor { get; set; }
    }

    public class Uint16ValidationRule : ValidationRule
    {
        public UInt16 Min { get; set; }
        public UInt16 Max { get; set; }
        public Uint16ValidationRule() { }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            UInt16 UInt32value = 0;
            try
            {
                if (((string)value).Length > 0)
                    UInt32value = UInt16.Parse((String)value);
            }
            catch
            {
                return new ValidationResult(false, "UInt16 values allowed");
            }
            if ((UInt32value < Min) || (UInt32value > Max))
            {
                return new ValidationResult(false, $"Please enter a valid UInt16 in the range: {Min}-{Max}.");
            }
            return ValidationResult.ValidResult;
        }
    }

    public class Uint32ValidationRule : ValidationRule
    {
        public UInt32 Min { get; set; }
        public UInt32 Max { get; set; }
        public Uint32ValidationRule() { }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            UInt32 UInt32value = 0;
            try
            {
                if (((string)value).Length > 0)
                    UInt32value = UInt32.Parse((String)value);
            }
            catch
            {
                return new ValidationResult(false, "UInt32 values allowed");
            }
            if ((UInt32value < Min) || (UInt32value > Max))
            {
                return new ValidationResult(false, $"Please enter a valid UInt32 in the range: {Min}-{Max}.");
            }
            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const UInt32 MAX_FILENAME_LENGTH = 128U;
        private int SIZEOF_HEADER;

        private ObservableCollection<ObsHeader> loadedFileObservible;
        private ObservableCollection<ObsHeader> SavedFileObservible;
        private ObservableCollection<ObsVersion> VersionObservible;

        private Dictionary<FileInfo, ACE_VF_Header.Header> FilesToSave = new Dictionary<FileInfo, ACE_VF_Header.Header>();
        private Dictionary<FileInfo, ACE_VF_Header.Header> FilesSaved = new Dictionary<FileInfo, ACE_VF_Header.Header>();

        public MainWindow()
        {
            InitializeComponent();
            loadedFileObservible = new ObservableCollection<ObsHeader>();
            SavedFileObservible = new ObservableCollection<ObsHeader>();
            VersionObservible = new ObservableCollection<ObsVersion>();

            load_listView.ItemsSource = loadedFileObservible;
            save_listView.ItemsSource = SavedFileObservible;
            Ver_Major_textBox.DataContext = VersionObservible;
            Ver_Minor_textBox.DataContext = VersionObservible;
            ACE_VF_Header.Header header = new ACE_VF_Header.Header();

            SIZEOF_HEADER = Marshal.SizeOf(header);
            
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog loadDialog = new OpenFileDialog();
            loadDialog.InitialDirectory = threedtd_shapefile_utility.Properties.Settings.Default.LoadDirectory;
            loadDialog.Filter = "csv files (*.csv)|*.csv";
            loadDialog.FilterIndex = 2;
            loadDialog.RestoreDirectory = true;
            loadDialog.Multiselect = true;
            loadDialog.Title = "Please Select Source File(s)";

            //If user selected files
            if (loadDialog.ShowDialog() == true)
            {
                //Clear out existing list
                var itemsSource = load_listView.ItemsSource as IList<ACE_VF_Header.Header>;
                if (itemsSource != null)
                {
                    itemsSource.Clear();
                }

                //Clear out collections
                loadedFileObservible.Clear();
                SavedFileObservible.Clear();
                VersionObservible.Clear();

                //Clear out saved data
                FilesToSave.Clear();
                FilesSaved.Clear();

                //Load selected files
                foreach (String file in loadDialog.FileNames)
                {
                    LoadSelectedData(file);
                }
                //Save the load file open path
                threedtd_shapefile_utility.Properties.Settings.Default.LoadDirectory = System.IO.Path.GetDirectoryName(loadDialog.FileName);
                threedtd_shapefile_utility.Properties.Settings.Default.Save();
            }
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            if (loadedFileObservible.Count > 0)
            {
                //prompt user for save directory
                using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
                {
                    fbd.SelectedPath = threedtd_shapefile_utility.Properties.Settings.Default.SaveDirectory;

                    System.Windows.Forms.DialogResult result = fbd.ShowDialog();

                    if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        SavedFileObservible.Clear();
                        FilesSaved.Clear();

                        if (Binary_radioButton.IsChecked == true)
                        {
                            SaveSelectedDataBin(fbd.SelectedPath);
                        }
                        else
                        {
                            SaveSelectedData(fbd.SelectedPath);
                        }
                        //Save file save path                
                        threedtd_shapefile_utility.Properties.Settings.Default.SaveDirectory = fbd.SelectedPath;
                        threedtd_shapefile_utility.Properties.Settings.Default.Save();
                    }
                }
            }
            else 
            {
                MessageBox.Show("Error: Load files before performing Save operation!");
            }
        }

        private void LoadedFilesDelete_Click(object sender, RoutedEventArgs e)
        {
            if (load_listView.SelectedIndex == -1)
            {
                return;
            }

            var itemsSource = load_listView.ItemsSource as IList<ACE_VF_Header.Header>;
            if (itemsSource != null)
            {
                itemsSource.RemoveAt(load_listView.SelectedIndex);
            }
        }

        private void Menu_Item_Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSelectedData(String file)
        {
            try
            {
                FileInfo info = new FileInfo(file);

                //If file opened successfully
                using (FileStream fs = File.OpenRead(info.FullName))
                {
                    try
                    {
                        ACE_CRC crc = new ACE_CRC();
                        UInt64 calcCRC = crc.getSeed();

                        byte[] buffer = new byte[info.Length];

                        int numRead = fs.Read(buffer, 0, (int)info.Length);
                        if (numRead != info.Length)
                        {
                            throw new Exception("Error: reading selected file");
                        }

                        uint numTriangles = 0;
                        //Get number of triangles (lines) in CSV file
                        using (var fsRead = new StreamReader(info.FullName))
                        {
                            //Now read file contents, collect floating point data to write
                            while (!fsRead.EndOfStream)
                            {
                                var line = fsRead.ReadLine();
                                if (line != null)
                                {
                                    numTriangles++;
                                }
                            }
                        }

                        ACE_VF_Header.Header header = new ACE_VF_Header.Header();

                        byte[] fileName;
                        if (TargetDir_textBox.Text != String.Empty)
                        {
                            fileName = StringToByteArray(TargetDir_textBox.Text + "\\" + info.Name, (uint)info.Name.Length);
                        }
                        else 
                        {
                            fileName = StringToByteArray(info.Name, (uint)info.Name.Length);
                        }
                        fileName = ByteArrayLeftPad(fileName, 0, (int)MAX_FILENAME_LENGTH);
                        //fill remainder with null termination

                        header.Filename = fileName;
                        header.Length = (UInt32)info.Length;
                        
                        if ((Ver_Major_textBox.Text == "") || (Ver_Minor_textBox.Text == ""))
                        {
                            Ver_Major_textBox.Text = "1";
                            Ver_Minor_textBox.Text = "0";
                        }
                        ObsVersion version = new ObsVersion(UInt16.Parse(Ver_Major_textBox.Text), UInt16.Parse(Ver_Minor_textBox.Text));
                        
                        header.UserData1 = (UInt32)(version.VerMajor << 16);
                        header.UserData1 |= (UInt32)version.VerMinor;
                        header.UserData2 = numTriangles;
                        header.Crc = 0;//Add CRC after calculating it

                        byte[] headerBytesMinusCRC = new byte[(ulong)SIZEOF_HEADER - sizeof(UInt64)];
                        
                        Buffer.BlockCopy(getBytes(header), sizeof(UInt64), headerBytesMinusCRC, 0, (int)SIZEOF_HEADER - sizeof(UInt64));

                        //Calculate CRC oh header and data
                        crc.CalculateCRC(headerBytesMinusCRC, (ulong)SIZEOF_HEADER - sizeof(UInt64), ref calcCRC);
                        crc.CalculateCRC(buffer, (ulong)info.Length, ref calcCRC);

                        //Set CRC
                        header.Crc = calcCRC;

                        //Add to files to save
                        FilesToSave.Add(info, header);

                        //Add to observable collection
                        ObsHeader obsHeader = new ObsHeader(header.Crc, Encoding.Default.GetString(header.Filename), header.UserData1, header.UserData2, header.Length);
                        loadedFileObservible.Add(obsHeader);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: reading selected files: " + ex.Message);
                    }
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. error: " + ex.Message);
            }
        }

        private byte[] ByteArrayLeftPad(byte[] input, byte padValue, int len)
        {
            var temp = Enumerable.Repeat(padValue, len).ToArray();
            for (var i = 0; i < input.Length; i++)
            {
                temp[i] = input[i];
            }
            return temp;
        }

        private void SaveSelectedData(String SelectedPath)
        {
            foreach (KeyValuePair<FileInfo, ACE_VF_Header.Header> fileToSave in FilesToSave)
            {
                try
                {
                    //If file to save opened successfully
                    using (FileStream fsRead = File.OpenRead(fileToSave.Key.FullName))
                    {
                        try
                        {

                            //Now read file contents
                            byte[] buffer = new byte[fileToSave.Value.Length];
                            int numRead = fsRead.Read(buffer, 0, (int)fileToSave.Value.Length);
                            if (numRead != fileToSave.Value.Length)
                            {
                                throw new Exception("Error: reading selected file");
                            }

                            //open write file
                            using (FileStream fsWrite = File.OpenWrite(SelectedPath + "\\" + fileToSave.Key.Name))
                            {
                                byte[] headerBytes = getBytes(fileToSave.Value);
                                //Write header information
                                fsWrite.Write(headerBytes, 0, headerBytes.Length);
                                //Write file information
                                fsWrite.Write(buffer, 0, (int)fileToSave.Value.Length);
                                fsWrite.Close();

                                FileInfo info = new FileInfo(SelectedPath + "\\" + fileToSave.Key.Name);

                                //Add to saved dictionary for validation
                                FilesSaved.Add(info, fileToSave.Value);

                                //Add to observable collection
                                ObsHeader obsHeader = new ObsHeader(fileToSave.Value.Crc, Encoding.Default.GetString(fileToSave.Value.Filename), fileToSave.Value.UserData1, fileToSave.Value.UserData2, fileToSave.Value.Length);
                                SavedFileObservible.Add(obsHeader);
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error: reading file. error: " + ex.Message);
                        }
                        fsRead.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not open file from disk. error: " + ex.Message);
                }
            }
        }

        private void SaveSelectedDataBin(String SelectedPath)
        {
            foreach (KeyValuePair<FileInfo, ACE_VF_Header.Header> fileToSave in FilesToSave)
            {
                //change csv file name to binary extension
                string fileName = Path.ChangeExtension(fileToSave.Key.Name, ".bin");

                //open write file
                using (FileStream fsWrite = File.OpenWrite(SelectedPath + "\\" + fileName))
                {
                    //If file to read opened successfully
                    using (var fsRead = new StreamReader(fileToSave.Key.FullName))
                    {
                        try
                        {
                            Collection<float> csvValues = new Collection<float>();

                            uint dataLength = 0;

                            //Now read file contents, collect floating point data to write
                            while (!fsRead.EndOfStream)
                            {
                                var line = fsRead.ReadLine();
                                var values = line.Split(',');
                                foreach (string val in values)
                                {
                                    float valFlt = float.Parse(val);
                                    csvValues.Add(valFlt);
                                    dataLength += sizeof(float);
                                }
                            }

                            //copy over any pertinent header user data. CRC and length will change when file converts to binary.
                            ACE_VF_Header.Header fileHeader = fileToSave.Value;

                            //Update file header with new length
                            fileHeader.Length = dataLength;

                            //Update file name to a binary extension
                            byte[] fileName2;
                            if (TargetDir_textBox.Text != String.Empty)
                            {
                                fileName2 = StringToByteArray(TargetDir_textBox.Text + "\\" + fileName, (uint)fileName.Length);
                            }
                            else
                            {
                                fileName2 = StringToByteArray(fileName, (uint)fileName.Length);
                            }
                            fileName2 = ByteArrayLeftPad(fileName2, 0, (int)MAX_FILENAME_LENGTH);
                            fileHeader.Filename = fileName2;

                            //Get Header write data
                            byte[] headerData;
                            headerData = getBytes(fileHeader);

                            //Get csv Write Data
                            byte[] csvData = new byte[dataLength];
                            int i = 0;
                            foreach(float val in csvValues)
                            {
                                Buffer.BlockCopy(BitConverter.GetBytes(val), 0, csvData, i, sizeof(float));
                                i += sizeof(float);
                            }

                            //Write data buffer is header plus csv data
                            byte[] writeData = new byte[SIZEOF_HEADER + dataLength];
                            Buffer.BlockCopy(headerData, 0, writeData, 0, (int)SIZEOF_HEADER);
                            Buffer.BlockCopy(csvData, 0, writeData, (int)SIZEOF_HEADER, csvData.Length);

                            ACE_CRC crc = new ACE_CRC();
                            UInt64 calcCRC = crc.getSeed();

                            //Copy of data minus CRC in header, used to calculate CRC
                            byte[] writeData2 = new byte[SIZEOF_HEADER + dataLength - sizeof(UInt64)];
                            Buffer.BlockCopy(writeData, sizeof(UInt64), writeData2, 0, (Int32)(SIZEOF_HEADER + dataLength - sizeof(UInt64)));

                            //calc CRC of data and header minus header CRC
                            crc.CalculateCRC(writeData2, ((uint)SIZEOF_HEADER - sizeof(UInt64)) + dataLength, ref calcCRC);

                            //Add new CRC to header
                            fileHeader.Crc = calcCRC;

                            //Get Header write data
                            byte[] headerData2;
                            headerData2 = getBytes(fileHeader);

                            //Re-add header to writeData
                            Buffer.BlockCopy(headerData2, 0, writeData, 0, (int)SIZEOF_HEADER);

                            //Write out the file
                            fsWrite.Write(writeData, 0, writeData.Length);

                            fsWrite.Close();

                            FileInfo info = new FileInfo(SelectedPath + "\\" + fileName);

                            //Add to saved dictionary for validation
                            FilesSaved.Add(info, fileHeader);

                            //Add to observable collection
                            ObsHeader obsHeader = new ObsHeader(fileHeader.Crc, Encoding.Default.GetString(fileHeader.Filename), fileHeader.UserData1, fileHeader.UserData2, fileHeader.Length);
                            SavedFileObservible.Add(obsHeader);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error: reading file. error: " + ex.Message);
                        }
                        fsRead.Close();
                    }
                }
            }
        }

        private void Validate_Button_Click(object sender, RoutedEventArgs e)
        {

            //Read in each saved file, calculate CRC, and validate calculated CRC against header CRC
            foreach (KeyValuePair<FileInfo, ACE_VF_Header.Header> fileSaved in FilesSaved)
            {
                //If file opened successfully
                using (FileStream fs = File.OpenRead(fileSaved.Key.FullName))
                {
                    try
                    {
                        ACE_CRC crc = new ACE_CRC();
                        UInt64 calcCRC = crc.getSeed();

                        //Read in csv file with header
                        byte[] buffer = new byte[fileSaved.Key.Length];

                        int numRead = fs.Read(buffer, 0, (int)(fileSaved.Key.Length));
                        if (numRead != (fileSaved.Key.Length))
                        {
                            throw new Exception("Error: reading selected file");
                        }

                        //Make copy of data minus the CRC in header
                        byte[] buffer2 = new byte[fileSaved.Key.Length - sizeof(UInt64)];
                        Buffer.BlockCopy(buffer, sizeof(UInt64), buffer2, 0, (Int32)(fileSaved.Key.Length - sizeof(UInt64)));

                        //Calculate CRC of data and header (ignore CRC in header)
                        crc.CalculateCRC(buffer2, (ulong)(fileSaved.Key.Length - sizeof(UInt64)), ref calcCRC);

                        //Get CRC from header of file to compare to calculated CRC
                        byte[] savedCRCArr = new byte[sizeof(UInt64)];
                        Buffer.BlockCopy(buffer, 0, savedCRCArr, 0, sizeof(UInt64));
                        UInt64 savedCRC = BitConverter.ToUInt64(savedCRCArr, 0);

                        //Mark data in observible collection as verified Calculated Matched Header
                        var item = SavedFileObservible.FirstOrDefault(i => i.Filename.Contains(fileSaved.Key.Name));
                        if (item.Crc != 0)
                        {
                            //If calculated CRC also matches CRC of header that was
                            //generated when files were loaded, prior to save. 
                            if (savedCRC == calcCRC)
                            {
                                SavedFileObservible.Remove(item);
                                item.Validated = "Ok";
                                SavedFileObservible.Add(item);
                            }

                            else
                            {
                                SavedFileObservible.Remove(item);
                                item.Validated = "ERR";
                                SavedFileObservible.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: reading selected files: " + ex.Message);
                    }
                    fs.Close();
                }
            }
        }

        private byte[] getBytes(ACE_VF_Header.Header data)
        {
            int size = (int)SIZEOF_HEADER;
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(data, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        private static byte[] StringToByteArray(string str, UInt32 length)
        {
            return Encoding.ASCII.GetBytes(str.PadRight((int)length, ' '));
        }

        private void Binary_RadioButton_clicked(object sender, RoutedEventArgs e)
        {

        }
    }
}
